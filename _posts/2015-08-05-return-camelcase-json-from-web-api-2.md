---
layout: post
title: "Return camelcase json from web api 2"
date: 2015-08-05 10:14:00
category: snippet
tags: webapi2 csharp json newtonsoft
credit: http://stackoverflow.com/questions/26474436/return-camelcased-json-from-web-api
---

In the `WebApiConfig.cs` file add the following lines and make sure the Newtonsoft Json.NET library is referenced.

```c#
JsonMediaTypeFormatter JsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
```