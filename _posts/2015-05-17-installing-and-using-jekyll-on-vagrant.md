---
layout: post
title:  "Installing and using jekyll on vagrant"
date:   2015-05-17 19:30:00
categories: post
tags: vagrant jekyll github-pages
credit: http://stackoverflow.com/questions/9885108/ssh-to-vagrant-box-in-windows
---

I wanted to document my experience installing and using Jekyll on Vagrant. For this I will be using the github-pages gem since I will be hosting my website from github.

## Vagrant port forwarding and provisioning

We will need to port forward `:4000` since this is what Jekyll serves the website on.

```ruby
# Located in Vagrantfile
config.vm.network :forwarded_port, host: 4000, guest: 4000
```

The above line simply tells vagrant to forward the virtual machine port `:4000` to the main pc's port `:4000` so you can go to `localhost:4000` to bring up the generated site.

We will also need to install the following tools during the vm provisioning step.

* git
* ruby
* bundler
* nodejs
* bower

## Github-pages gem

I installed the github-pages gem per Github's instructions, [here](https://help.github.com/articles/using-jekyll-with-pages/).

## Using jekyll

Once you have a jekyll site created, you can use Github's suggestion of executing the following command to serve the site.

```sh
bundle exec jekyll serve
```

But the problem I ran into was even though it said auto regeneration was enabled, it in fact was not regenerating after a change was made. I came across [this Stack Overflow answer](http://stackoverflow.com/questions/23774304/jekyll-server-on-but-not-regenerating) which suggests to add `--force_polling`.

```sh
bundle exec jekyll serve --force_polling
```

It works ... sometimes and is somewhat slow. But hey its a better solution than stopping and starting the jekyll server for each change.
