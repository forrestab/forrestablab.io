---
layout: post
title:  "Using vagrant ssh on windows"
date:   2015-05-16 01:36:00
categories: post
tags: vagrant windows ssh git
credit: http://stackoverflow.com/questions/9885108/ssh-to-vagrant-box-in-windows
---

This is an extra step in order to get the `vagrant ssh` command to work in Windows cmd. Very simple, if you don't have git installed, install it. Once it is installed add git's bin directory to the `%PATH%` environment variable.

```
set PATH=%PATH%;C:\Program Files (x86)\Git\bin
```
