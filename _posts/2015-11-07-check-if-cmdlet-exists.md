---
layout: post
title:  "Check if cmdlet exists"
date:   2015-11-07 15:04:00
categories: snippet
tags: powershell
credit: http://stackoverflow.com/questions/3919798/how-to-check-if-a-cmdlet-exists-in-powershell-at-runtime-via-script
---

```
[bool](Get-Command -Name "Invoke-Sqlcmd" -ErrorAction SilentlyContinue)
```

The above command just simply interprets the result of `Get-Command` as a boolean. Essentially `[bool]` will see any nonempty string as true and any empty string as false ([see comment by **Hemant**](https://connect.microsoft.com/PowerShell/feedback/details/298020/all-string-values-convert-to-boolean-true)).
