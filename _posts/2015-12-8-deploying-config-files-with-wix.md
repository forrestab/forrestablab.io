---
layout: post
title:  "Deploying config files with WiX"
date:   2015-12-08 11:20:00
categories: snippet
tags: wix
credit: http://stackoverflow.com/questions/2441651/wix-overwrites-config-files-during-setup-how-can-i-avoid-this
---

```xml
<!-- Using a combination of NeverOverwrite and KeyPath, you can avoid overwriting files on update -->
<Component Id="C.ServiceConfig" Guid="151fbb78-9331-4fe6-aa01-bfaea9368e95" NeverOverwrite="yes">
    <File Id="Fi.ServiceConfig" KeyPath="yes" Source="$(var.Service.TargetDir)\Service.config" />
</Component>
```

To make sure a config file is not overwritten on an update, make sure the `NeverOverwrite` attribute on the *Component* is set to yes. 
