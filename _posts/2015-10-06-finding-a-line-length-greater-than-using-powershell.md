---
layout: post
title:  "Finding a line length greater than using powershell"
date:   2015-10-06 11:07:00
categories: snippet
tags: powershell
credit: http://stackoverflow.com/questions/12520942/use-powershell-to-determine-length-of-a-line-in-a-text-file
---

I had this 60,000+ transaction file and needed to find the one transaction whos length was greater than the rest. Tried eyeballing it and found nothing; was about to write a quick C# console app when I decided to try out Powershell.

I came across this [one line command](http://stackoverflow.com/questions/12520942/use-powershell-to-determine-length-of-a-line-in-a-text-file#comment16857296_12521079) in the comments of the correct answer and modified it slightly to print the transaction instead of a message.

```
Get-Content "C:\FileToLookAt.txt" | ? { $_.length -gt 272 } | % { Write-Host $_ }
```
